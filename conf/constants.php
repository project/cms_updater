<?php
/**
 * @file
 * Contains constant definitions for CMS Updater
 */

// Define the CMS Updater service url.
define('CMS_UPDATER_SERVICE_URL', 'https://cms-updater.com');
// Define Drupal 7
define('CMS_UPDATER_CMS_ID', 1);
define('CMS_UPDATER_CLASS_PREFIX', 'Drupal7');